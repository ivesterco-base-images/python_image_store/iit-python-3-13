FROM python:3.13.0a2-bookworm
WORKDIR /app
COPY ./requirements.txt /app
RUN pip install -r requirements.txt
COPY . .
EXPOSE 8080
ENV FLASK_APP=hello_world.py
CMD ["flask", "run", "--host", "0.0.0.0"]
